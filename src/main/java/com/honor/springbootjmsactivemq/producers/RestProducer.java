package com.honor.springbootjmsactivemq.producers;

import javax.jms.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/publish")
public class RestProducer {
	
	@Autowired
	private Queue queue;
	
	/*
	 * JmsTemplate instance is created and injected by SpringBoot. 
	 * As in the case for connection factory (Embedded ActiveMQ case).
	 * */
	/*
	 * SpringBoot also creates and injects the connection factory when we are using in-memory ActiveMQ.
	 * JmsTemplate instance is created with that default connection factory.
	 * */
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@GetMapping("/{message}")
	public String publish(@PathVariable("message") final String message) {
		jmsTemplate.convertAndSend(queue, message);
		return "published: " + message;
	}
}
