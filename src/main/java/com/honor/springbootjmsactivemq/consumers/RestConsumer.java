package com.honor.springbootjmsactivemq.consumers;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class RestConsumer {
	
	@JmsListener(destination="inmemory.queue")
	public void listen(String message) {
		System.out.println("consumed: " + message);
	}
}
